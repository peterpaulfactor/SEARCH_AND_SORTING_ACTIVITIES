def recursion_binary_search(numbers, left, right, num):
    if left <= right:
        mid = (left+right) // 2 

        if numbers[mid] == num:
            return mid
        elif numbers[mid] > num:
            return recursion_binary_search(numbers, left, mid -1, num)
        else:
            return recursion_binary_search(numbers, mid +1, right, num)
    else:
        return -1
numbers =[23, 10, 5, 80, 50]
num = 80

print(recursion_binary_search(numbers, 0, len(numbers)-1, num))