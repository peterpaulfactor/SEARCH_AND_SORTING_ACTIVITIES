def binary_search(numbers, num):
    left = 0
    right = len(numbers)
    while left <=right:
        mid =(left+right) // 2

        if numbers [mid] == num:
            return mid
        elif numbers [mid] > num:
            right = mid -1
        else:
            left = mid +1 
    return -1           
numbers =[34, 22, 35, 44, 50]
num = 5
print(binary_search(numbers, num))