def while_linear_search(numbers, num):
    pointer = 0
    while pointer < len(numbers):
        if numbers[pointer] == num:
            return pointer
        else:
            pointer+=1
    return -1            
numbers =[23, 40, 34, 37, 24]
num = 34        

print(while_linear_search(numbers, num))