def bubble_sort(numbers):
  length = len(numbers)
  for i in range(length):
    for j in range(length - 1):
        if numbers[j]>numbers[j + 1]:
            numbers[j], numbers[j + 1] = numbers[j + 1], numbers[j]
    return numbers
numbers=[12, 27, 25, 24, 32, 27]
print(bubble_sort(numbers))            